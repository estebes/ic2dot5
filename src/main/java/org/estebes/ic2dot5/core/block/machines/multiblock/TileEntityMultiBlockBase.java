package org.estebes.ic2dot5.core.block.machines.multiblock;

import ic2.core.IC2;
import ic2.core.block.TileEntityBlock;
import net.minecraft.nbt.NBTTagCompound;
import org.estebes.ic2dot5.core.block.machines.io.TileEntityInputBus;
import org.estebes.ic2dot5.core.block.machines.io.TileEntityOutputBus;

import java.util.ArrayList;
import java.util.List;

/**
 * Author: Vitor Andrade
 * Date: 8/17/18
 * Time: 8:57 AM
 */

public abstract class TileEntityMultiBlockBase extends TileEntityBlock {
    // NBT >>
    @Override
    public void readFromNBT(NBTTagCompound nbtTagCompound) {
        super.readFromNBT(nbtTagCompound);
        this.progress = nbtTagCompound.getInteger("progress");
    }

    @Override
    public NBTTagCompound writeToNBT(NBTTagCompound nbtTagCompound) {
        super.writeToNBT(nbtTagCompound);
        nbtTagCompound.setInteger("progress", this.progress);
        return nbtTagCompound;
    }
    // << NBT

    @Override
    protected void onLoaded() {
        super.onLoaded();

        if (!this.getWorld().isRemote) {
            mbInputBuses.clear();
            mbOutputBuses.clear();
        }
    }

    // Multi-Block logic >>
    protected abstract void lookupMB();
    // << Multi-Block logic

    // Fields >>
    // Tick rate >>
    protected final int tickRate = 20;
    protected int ticker = IC2.random.nextInt(tickRate);
    // << Tick rate

    // Progress >>
    protected int progress = 0;
    // << Progress

    // Multi-Block parts >>
    protected List<TileEntityInputBus> mbInputBuses = new ArrayList<>();
    protected List<TileEntityOutputBus> mbOutputBuses = new ArrayList<>();
    // << Multi-Block parts
    // << Fields
}
