package org.estebes.ic2dot5.core.block;

import ic2.core.IC2;
import ic2.core.block.BlockWall;
import ic2.core.block.TileEntityBlock;
import ic2.core.block.TileEntityWall;
import ic2.core.block.comp.Obscuration;
import ic2.core.block.state.Ic2BlockState;
import ic2.core.block.state.UnlistedProperty;
import ic2.core.network.NetworkManager;
import ic2.core.ref.BlockName;
import ic2.core.util.Ic2Color;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.EnumDyeColor;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.math.RayTraceResult;
import net.minecraftforge.common.property.IUnlistedProperty;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Author: Vitor Andrade
 * Date: 8/16/18
 * Time: 9:10 AM
 */

public class TileEntityShitTesting extends TileEntityBlock {
    public static final IUnlistedProperty<TileEntityWall.WallRenderState> renderStateProperty = new UnlistedProperty("renderstate", TileEntityWall.WallRenderState.class);
    protected final Obscuration obscuration;
    private Ic2Color color;
    private volatile TileEntityWall.WallRenderState renderState;

    public TileEntityShitTesting() {
        this(BlockWall.defaultColor);
    }

    public TileEntityShitTesting(Ic2Color color) {
        this.color = BlockWall.defaultColor;
        this.obscuration = (Obscuration)this.addComponent(new Obscuration(this, new Runnable() {
            public void run() {
                ((NetworkManager) IC2.network.get(true)).updateTileEntityField(TileEntityShitTesting.this, "obscuration");
            }
        }));
        this.color = color;
    }

    public void readFromNBT(NBTTagCompound nbt) {
        super.readFromNBT(nbt);
        this.color = Ic2Color.values[nbt.getByte("color") & 255];
    }

    public NBTTagCompound writeToNBT(NBTTagCompound nbt) {
        super.writeToNBT(nbt);
        nbt.setByte("color", (byte)this.color.ordinal());
        return nbt;
    }

    protected void onLoaded() {
        super.onLoaded();
        if (this.getWorld().isRemote) {
            this.updateRenderState();
        }

    }

    protected Ic2BlockState.Ic2BlockStateInstance getExtendedState(Ic2BlockState.Ic2BlockStateInstance state) {
        state = super.getExtendedState(state);
        TileEntityWall.WallRenderState value = this.renderState;
        if (value != null) {
            state = state.withProperties(new Object[]{renderStateProperty, value});
        }

        return state;
    }

    public List<String> getNetworkedFields() {
        List<String> ret = new ArrayList();
        ret.add("color");
        ret.add("obscuration");
        ret.addAll(super.getNetworkedFields());
        return ret;
    }

    public void onNetworkUpdate(String field) {
        super.onNetworkUpdate(field);
        if (this.updateRenderState()) {
            this.rerender();
        }

    }

    protected boolean recolor(EnumFacing side, EnumDyeColor mcColor) {
        Ic2Color color = Ic2Color.get(mcColor);
        if (color == this.color) {
            return false;
        } else {
            this.color = color;
            if (!this.getWorld().isRemote) {
                ((NetworkManager)IC2.network.get(true)).updateTileEntityField(this, "obscuration");
                this.markDirty();
            } else if (this.updateRenderState()) {
                this.rerender();
            }

            return true;
        }
    }

    protected ItemStack getPickBlock(EntityPlayer player, RayTraceResult target) {
        return BlockName.wall.getItemStack(this.color);
    }

    protected boolean clientNeedsExtraModelInfo() {
        return this.obscuration.hasObscuration();
    }

    private boolean updateRenderState() {
        TileEntityWall.WallRenderState state = new TileEntityWall.WallRenderState(this.color, this.obscuration.getRenderState());
        if (state.equals(this.renderState)) {
            return false;
        } else {
            this.renderState = state;
            return true;
        }
    }
}
