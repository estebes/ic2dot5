//package org.estebes.ic2dot5.core.block;
//
//import ic2.core.block.state.Ic2BlockState.Ic2BlockStateInstance;
//import ic2.core.model.AbstractModel;
//import ic2.core.model.BasicBakedBlockModel;
//import ic2.core.model.ISpecialParticleModel;
//import ic2.core.model.MergedBlockModel;
//import ic2.core.model.ModelUtil;
//import ic2.core.model.VdUtil;
//import ic2.core.ref.BlockName;
//import net.minecraft.block.state.IBlockState;
//import net.minecraft.client.renderer.block.model.BakedQuad;
//import net.minecraft.client.renderer.block.model.IBakedModel;
//import net.minecraft.client.renderer.texture.TextureAtlasSprite;
//import net.minecraft.util.EnumFacing;
//import org.estebes.ic2dot5.core.block.TileEntityShitTesting.HatchRenderState;
//
//import java.nio.IntBuffer;
//import java.util.ArrayList;
//import java.util.Arrays;
//import java.util.List;
//
///**
// * Author: Vitor Andrade
// * Date: 8/16/18
// * Time: 9:09 AM
// */
//
//public class RenderBlockWall extends AbstractModel implements ISpecialParticleModel {
//    public RenderBlockWall() {
//    }
//
//    public List<BakedQuad> getQuads(IBlockState rawState, EnumFacing side, long rand) {
//        if (!(rawState instanceof Ic2BlockStateInstance)) {
//            return ModelUtil.getMissingModel().getQuads(rawState, side, rand);
//        } else {
//            Ic2BlockStateInstance state = (Ic2BlockStateInstance)rawState;
//            if (!state.hasValue(TileEntityShitTesting.renderStateProperty)) {
//                return ModelUtil.getMissingModel().getQuads(state, side, rand);
//            } else {
//                HatchRenderState prop = (HatchRenderState) state.getValue(TileEntityShitTesting.renderStateProperty);
//                float[][] uvs = new float[6][];
//                int[][] colorMultipliers = new int[6][];
//                TextureAtlasSprite[][] sprites = new TextureAtlasSprite[6][];
//                int total = 0;
//
//                IBakedModel baseModel = ModelUtil.getBlockModel(BlockName.wall.getBlockState(prop.color));
//                if (total == 0) {
//                    return baseModel.getQuads(state, side, rand);
//                } else {
//                    MergedBlockModel mergedModel = generateModel(baseModel, state, colorMultipliers);
//                    mergedModel.setSprite(uvs, colorMultipliers, sprites);
//                    return mergedModel.getQuads(state, side, rand);
//                }
//            }
//        }
//    }
//
//    private static MergedBlockModel generateModel(IBakedModel baseModel, IBlockState state, int[][] colorMultipliers) {
//        float offset = 0.001F;
//        List<BakedQuad>[] mergedQuads = new List[6];
//        int[] retextureStart = new int[6];
//        IntBuffer buffer = VdUtil.getQuadBuffer();
//        EnumFacing[] var7 = EnumFacing.VALUES;
//        int var8 = var7.length;
//
//        for(int var9 = 0; var9 < var8; ++var9) {
//            EnumFacing side = var7[var9];
//            int[] sideColorMultipliers = colorMultipliers[side.ordinal()];
//            List<BakedQuad> baseFaceQuads = baseModel.getQuads(state, side, 0L);
//            if (sideColorMultipliers == null) {
//                mergedQuads[side.ordinal()] = baseFaceQuads;
//            } else {
//                List<BakedQuad> mergedFaceQuads = new ArrayList(baseFaceQuads.size() + sideColorMultipliers.length);
//                mergedFaceQuads.addAll(baseFaceQuads);
//                int[] var14 = sideColorMultipliers;
//                int var15 = sideColorMultipliers.length;
//
//                for(int var16 = 0; var16 < var15; ++var16) {
//                    int var10000 = var14[var16];
//                    generateQuad(side, 0.001F, buffer);
//                    mergedFaceQuads.add(BasicBakedBlockModel.createQuad(Arrays.copyOf(buffer.array(), buffer.position()), side, (TextureAtlasSprite)null));
//                    buffer.rewind();
//                }
//
//                mergedQuads[side.ordinal()] = mergedFaceQuads;
//            }
//
//            retextureStart[side.ordinal()] = baseFaceQuads.size();
//        }
//
//        return new MergedBlockModel(baseModel, mergedQuads, retextureStart);
//    }
//
//    private static void generateQuad(EnumFacing side, float offset, IntBuffer out) {
//        float neg = -offset;
//        float pos = 1.0F + offset;
//        switch(side) {
//            case DOWN:
//                VdUtil.generateBlockVertex(neg, neg, neg, 0.0F, 0.0F, side, out);
//                VdUtil.generateBlockVertex(pos, neg, neg, 1.0F, 0.0F, side, out);
//                VdUtil.generateBlockVertex(pos, neg, pos, 1.0F, 1.0F, side, out);
//                VdUtil.generateBlockVertex(neg, neg, pos, 0.0F, 1.0F, side, out);
//                break;
//            case UP:
//                VdUtil.generateBlockVertex(neg, pos, neg, 0.0F, 0.0F, side, out);
//                VdUtil.generateBlockVertex(neg, pos, pos, 0.0F, 1.0F, side, out);
//                VdUtil.generateBlockVertex(pos, pos, pos, 1.0F, 1.0F, side, out);
//                VdUtil.generateBlockVertex(pos, pos, neg, 1.0F, 0.0F, side, out);
//                break;
//            case NORTH:
//                VdUtil.generateBlockVertex(neg, neg, neg, 0.0F, 0.0F, side, out);
//                VdUtil.generateBlockVertex(neg, pos, neg, 0.0F, 1.0F, side, out);
//                VdUtil.generateBlockVertex(pos, pos, neg, 1.0F, 1.0F, side, out);
//                VdUtil.generateBlockVertex(pos, neg, neg, 1.0F, 0.0F, side, out);
//                break;
//            case SOUTH:
//                VdUtil.generateBlockVertex(neg, neg, pos, 0.0F, 0.0F, side, out);
//                VdUtil.generateBlockVertex(pos, neg, pos, 1.0F, 0.0F, side, out);
//                VdUtil.generateBlockVertex(pos, pos, pos, 1.0F, 1.0F, side, out);
//                VdUtil.generateBlockVertex(neg, pos, pos, 0.0F, 1.0F, side, out);
//                break;
//            case WEST:
//                VdUtil.generateBlockVertex(neg, neg, neg, 0.0F, 0.0F, side, out);
//                VdUtil.generateBlockVertex(neg, neg, pos, 1.0F, 0.0F, side, out);
//                VdUtil.generateBlockVertex(neg, pos, pos, 1.0F, 1.0F, side, out);
//                VdUtil.generateBlockVertex(neg, pos, neg, 0.0F, 1.0F, side, out);
//                break;
//            case EAST:
//                VdUtil.generateBlockVertex(pos, neg, neg, 0.0F, 0.0F, side, out);
//                VdUtil.generateBlockVertex(pos, pos, neg, 0.0F, 1.0F, side, out);
//                VdUtil.generateBlockVertex(pos, pos, pos, 1.0F, 1.0F, side, out);
//                VdUtil.generateBlockVertex(pos, neg, pos, 1.0F, 0.0F, side, out);
//                break;
//            default:
//                throw new IllegalArgumentException();
//        }
//
//    }
//
//    public void onReload() {
//    }
//
//    public TextureAtlasSprite getParticleTexture(Ic2BlockStateInstance state) {
//        if (!state.hasValue(TileEntityShitTesting.renderStateProperty)) {
//            return ModelUtil.getMissingModel().getParticleTexture();
//        } else {
//            HatchRenderState prop = (HatchRenderState) state.getValue(TileEntityShitTesting.renderStateProperty);
//            return ModelUtil.getBlockModel(BlockName.wall.getBlockState(prop.color)).getParticleTexture();
//        }
//    }
//}
//
