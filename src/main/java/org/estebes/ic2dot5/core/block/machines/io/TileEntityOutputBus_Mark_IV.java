package org.estebes.ic2dot5.core.block.machines.io;

/**
 * Author: Vitor Andrade
 * Date: 8/17/18
 * Time: 8:51 AM
 */

public class TileEntityOutputBus_Mark_IV extends TileEntityOutputBus {
    public TileEntityOutputBus_Mark_IV() {
        super(4, 16);
    }
}
