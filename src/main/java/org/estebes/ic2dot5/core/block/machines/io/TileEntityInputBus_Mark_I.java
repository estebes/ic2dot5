package org.estebes.ic2dot5.core.block.machines.io;

/**
 * Author: Vitor Andrade
 * Date: 8/17/18
 * Time: 9:12 AM
 */

public class TileEntityInputBus_Mark_I extends TileEntityInputBus {
    public TileEntityInputBus_Mark_I() {
        super(1);
    }
}