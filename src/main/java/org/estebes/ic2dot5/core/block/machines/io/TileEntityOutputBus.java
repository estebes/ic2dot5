package org.estebes.ic2dot5.core.block.machines.io;

import ic2.core.ContainerBase;
import ic2.core.IHasGui;
import ic2.core.block.TileEntityInventory;
import ic2.core.block.invslot.InvSlotOutput;
import ic2.core.gui.dynamic.DynamicContainer;
import ic2.core.gui.dynamic.DynamicGui;
import ic2.core.gui.dynamic.GuiParser;
import net.minecraft.client.gui.GuiScreen;
import net.minecraft.entity.player.EntityPlayer;

/**
 * Author: Vitor Andrade
 * Date: 8/16/18
 * Time: 10:29 PM
 */

public abstract class TileEntityOutputBus extends TileEntityInventory implements IHasGui {
    public TileEntityOutputBus(int tier, int slotCount) {
        this.tier = tier;
        this.outputSlot = new InvSlotOutput(this, "outputSlot", slotCount);
    }

    // IHasGui >>
    @Override
    public ContainerBase<? extends TileEntityOutputBus> getGuiContainer(EntityPlayer entityPlayer) {
        return DynamicContainer.create(this, entityPlayer, GuiParser.parse(this.teBlock));
    }

    @Override
    public GuiScreen getGui(EntityPlayer entityPlayer, boolean b) {
        return DynamicGui.create(this, entityPlayer, GuiParser.parse(this.teBlock));
    }

    @Override
    public void onGuiClosed(EntityPlayer entityPlayer) {
        // NO-OP
    }
    // << IHasGui

    // Fields >>
    public final int tier;
    public final InvSlotOutput outputSlot;
    // << Fields
}