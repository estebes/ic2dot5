package org.estebes.ic2dot5.core.block.machines.io;

/**
 * Author: Vitor Andrade
 * Date: 8/17/18
 * Time: 9:14 AM
 */

public class TileEntityInputBus_Mark_IV extends TileEntityInputBus {
    public TileEntityInputBus_Mark_IV() {
        super(4);
    }
}
