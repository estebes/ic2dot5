package org.estebes.ic2dot5.core.block.misc;

import ic2.core.block.BlockMultiID;
import ic2.core.block.state.IIdProvider;
import ic2.core.block.type.IBlockSound;
import ic2.core.block.type.IExtBlockType;
import ic2.core.init.BlocksItems;
import ic2.core.item.block.ItemBlockMulti;
import net.minecraft.block.SoundType;
import net.minecraft.block.material.Material;
import net.minecraft.block.state.IBlockState;
import net.minecraft.item.Item;
import net.minecraft.util.ResourceLocation;
import org.estebes.ic2dot5.core.ref.ModInfo;

import static java.util.Locale.ENGLISH;

/**
 * Author: Vitor Andrade
 * Date: 8/17/18
 * Time: 7:32 PM
 */

public class BlockCasing extends BlockMultiID<BlockCasing.Materials> {
    // Magic >>
    public static BlockCasing create() {
        BlockCasing ret = (BlockCasing) BlockMultiID.create(BlockCasing.class, BlockCasing.Materials.class, new Object[0]);
        BlocksItems.registerItem((Item) createItemBlockSupplier(ItemBlockMulti.class).apply(ret), new ResourceLocation(ModInfo.MOD_ID, NAME)).setUnlocalizedName(ret.getUnlocalizedName());
        return ret;
    }
    // << Magic

    public BlockCasing() {
        super(null, Material.IRON);

        this.setUnlocalizedName(NAME);
//        BlocksItems.registerItem((Item) createItemBlockSupplier(ItemBlockMulti.class).apply(this), new ResourceLocation(ModInfo.MOD_ID, NAME)).setUnlocalizedName(this.getUnlocalizedName());
        BlocksItems.registerBlock(this, new ResourceLocation(ModInfo.MOD_ID, NAME));

        //BlocksItems.registerItem(new ItemBlockMulti(this), new ResourceLocation(ModInfo.MOD_ID, NAME)).setUnlocalizedName(NAME);
    }

    @Override
    public int damageDropped(IBlockState state) {
        return this.getMetaFromState(state);
    }

    // Fields >>
    protected static final String NAME = "casing";
    // << Fields

    public static enum Materials implements IIdProvider, IExtBlockType, IBlockSound {
        aluminium(5.0F, 5.0F),
        bronze(5.0F, 5.0F),
        copper(5.0F, 5.0F),
        iridium(5.0F, 5.0F),
        iron(5.0F, 5.0F),
        lead(5.0F, 5.0F),
        stainless_steel(5.0F, 5.0F),
        steel(5.0F, 5.0F),
        tin(5.0F, 5.0F)
        ;

        private final String name;

        Materials(float hardness, float explosionResistance) {
            this(hardness, explosionResistance, SoundType.METAL);
        }

        Materials(float hardness, float explosionResistance, SoundType sound) {
            this.hardness = hardness;
            this.explosionResistance = explosionResistance;
            this.sound = sound;
            this.name = name().toLowerCase(ENGLISH);
        }

        @Override
        public String getName() {
            return this.name;
        }

        @Override
        public int getId() {
            return this.ordinal();
        }

        @Override
        public SoundType getSound() {
            return this.sound;
        }

        @Override
        public float getHardness() {
            return this.hardness;
        }

        @Override
        public float getExplosionResistance() {
            return this.explosionResistance;
        }

        // Fields >>
        private final float hardness;
        private final float explosionResistance;
        private final SoundType sound;
        // << Fields
    }
}
