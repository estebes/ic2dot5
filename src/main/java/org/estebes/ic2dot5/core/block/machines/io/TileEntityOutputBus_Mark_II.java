package org.estebes.ic2dot5.core.block.machines.io;

/**
 * Author: Vitor Andrade
 * Date: 8/17/18
 * Time: 8:51 AM
 */

public class TileEntityOutputBus_Mark_II extends TileEntityOutputBus {
    public TileEntityOutputBus_Mark_II() {
        super(2, 4);
    }
}
