//package org.estebes.ic2dot5.core.ref;
//
//import ic2.core.block.state.IIdProvider;
//import ic2.core.block.type.IBlockSound;
//import ic2.core.block.type.IExtBlockType;
//import net.minecraft.block.SoundType;
//
//import static java.util.Locale.ENGLISH;
//
///**
// * Author: Vitor Andrade
// * Date: 8/17/18
// * Time: 7:34 PM
// */
//
//public enum Materials implements IIdProvider, IExtBlockType, IBlockSound {
//    aluminium(5.0F, 5.0F),
//    bronze(5.0F, 5.0F),
//    copper(5.0F, 5.0F),
//    iridium(5.0F, 5.0F),
//    iron(5.0F, 5.0F),
//    lead(5.0F, 5.0F),
//    stainless_steel(5.0F, 5.0F),
//    steel(5.0F, 5.0F),
//    tin(5.0F, 5.0F)
//    ;
//
//    private final String name;
//
//    Materials(float hardness, float explosionResistance) {
//        this(hardness, explosionResistance, SoundType.METAL);
//    }
//
//    Materials(float hardness, float explosionResistance, SoundType sound) {
//        this.hardness = hardness;
//        this.explosionResistance = explosionResistance;
//        this.sound = sound;
//        this.name = name().toLowerCase(ENGLISH);
//    }
//
//    @Override
//    public String getName() {
//        return this.name;
//    }
//
//    @Override
//    public int getId() {
//        return this.ordinal();
//    }
//
//    @Override
//    public SoundType getSound() {
//        return this.sound;
//    }
//
//    @Override
//    public float getHardness() {
//        return this.hardness;
//    }
//
//    @Override
//    public float getExplosionResistance() {
//        return this.explosionResistance;
//    }
//
//    // Fields >>
//    private final float hardness;
//    private final float explosionResistance;
//    private final SoundType sound;
//    // << Fields
//}
