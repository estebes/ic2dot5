package org.estebes.ic2dot5.core.ref

object ModInfo {
    const val MOD_ID: String = "ic2dot5"
    const val MOD_NAME: String = "IC2dot5"
    const val MOD_VERSION: String = "0.1"
}