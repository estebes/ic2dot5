package org.estebes.ic2dot5.core.ref

import ic2.core.block.BlockTileEntity
import ic2.core.block.ITeBlock
import ic2.core.block.TileEntityBlock
import ic2.core.item.block.ItemBlockTileEntity
import ic2.core.ref.TeBlock
import ic2.core.util.Util
import net.minecraft.block.material.Material
import net.minecraft.creativetab.CreativeTabs
import net.minecraft.item.EnumRarity
import net.minecraft.item.ItemStack
import net.minecraft.util.EnumFacing
import net.minecraft.util.NonNullList
import net.minecraft.util.ResourceLocation
import net.minecraftforge.fml.common.registry.GameRegistry
import net.minecraftforge.fml.relauncher.Side
import net.minecraftforge.fml.relauncher.SideOnly
import org.estebes.ic2dot5.core.block.TileEntityShitTesting
import org.estebes.ic2dot5.core.block.machines.io.TileEntityInputBus_Mark_I
import org.estebes.ic2dot5.core.block.machines.io.TileEntityInputBus_Mark_II
import org.estebes.ic2dot5.core.block.machines.io.TileEntityInputBus_Mark_III
import org.estebes.ic2dot5.core.block.machines.io.TileEntityInputBus_Mark_IV
import org.estebes.ic2dot5.core.block.machines.io.TileEntityOutputBus_Mark_I
import org.estebes.ic2dot5.core.block.machines.io.TileEntityOutputBus_Mark_II
import org.estebes.ic2dot5.core.block.machines.io.TileEntityOutputBus_Mark_III
import org.estebes.ic2dot5.core.block.machines.io.TileEntityOutputBus_Mark_IV
import org.estebes.ic2dot5.core.tileentity.TileEntityCentrifuge
import org.estebes.ic2dot5.core.tileentity.TileEntityCrusher
import org.estebes.ic2dot5.core.tileentity.TileEntityMultiBlockItemInputHatch
import org.estebes.ic2dot5.core.tileentity.multiblock.TileEntityMultiBlockCrusher

@Suppress("EnumEntryName")
enum class TEs(
        val _tileEntityClass: Class<out TileEntityBlock>,
        val _itemMeta: Int,
        val _hasActive: Boolean = false,
        val _supportedFacings: MutableSet<EnumFacing> = Util.horizontalFacings,
        val _allowWrenchRotating: Boolean = false,
        val _harvestTool: TeBlock.HarvestTool = TeBlock.HarvestTool.Pickaxe,
        val _defaultDrop: TeBlock.DefaultDrop = TeBlock.DefaultDrop.Self,
        val _hardness: Float = 3F,
        val _explosionResistance: Float = 15F,
        val _rarity: EnumRarity
) : ITeBlock, ITeBlock.ITeBlockCreativeRegisterer {
    xtbx_crusher(_tileEntityClass = TileEntityCrusher::class.java, _itemMeta = 1, _rarity = EnumRarity.COMMON),
    xtbx_centrifuge(_tileEntityClass = TileEntityCentrifuge::class.java, _itemMeta = 2, _rarity = EnumRarity.COMMON,
            _supportedFacings = Util.allFacings, _allowWrenchRotating = true),
    xtbx_input_hatch(_tileEntityClass = TileEntityMultiBlockItemInputHatch::class.java, _itemMeta = 4, _rarity = EnumRarity.COMMON),

    // 100 - 109 - Output Buses >>
    ic2dot5_output_bus_tier_1(_tileEntityClass = TileEntityOutputBus_Mark_I::class.java, _itemMeta = 100, _rarity = EnumRarity.COMMON),
    ic2dot5_output_bus_tier_2(_tileEntityClass = TileEntityOutputBus_Mark_II::class.java, _itemMeta = 101, _rarity = EnumRarity.COMMON),
    ic2dot5_output_bus_tier_3(_tileEntityClass = TileEntityOutputBus_Mark_III::class.java, _itemMeta = 102, _rarity = EnumRarity.COMMON),
    ic2dot5_output_bus_tier_4(_tileEntityClass = TileEntityOutputBus_Mark_IV::class.java, _itemMeta = 103, _rarity = EnumRarity.COMMON),
    // << 100 - 109 - Output Buses

    // 110 - 119 - Input Buses >>
    ic2dot5_input_bus_tier_1(_tileEntityClass = TileEntityInputBus_Mark_I::class.java, _itemMeta = 110, _rarity = EnumRarity.COMMON),
    ic2dot5_input_bus_tier_2(_tileEntityClass = TileEntityInputBus_Mark_II::class.java, _itemMeta = 111, _rarity = EnumRarity.COMMON),
    ic2dot5_input_bus_tier_3(_tileEntityClass = TileEntityInputBus_Mark_III::class.java, _itemMeta = 112, _rarity = EnumRarity.COMMON),
    ic2dot5_input_bus_tier_4(_tileEntityClass = TileEntityInputBus_Mark_IV::class.java, _itemMeta = 113, _rarity = EnumRarity.COMMON),
    // << 110 - 119 - Input Buses

    // 200 - 299 - Controllers >>
    ic2dot5_mb_crusher_tier_1(_tileEntityClass = TileEntityMultiBlockCrusher::class.java, _itemMeta = 200, _hasActive = true, _rarity = EnumRarity.COMMON),
    // << 200 - 299 - Controllers

    ic2dot5_wall(TileEntityShitTesting::class.java, -1, false, Util.noFacings, false, TeBlock.HarvestTool.Pickaxe, TeBlock.DefaultDrop.Self, 3.0f, 30.0f, EnumRarity.COMMON),
    ;

    init {
        GameRegistry.registerTileEntity(_tileEntityClass, ModInfo.MOD_ID + name)
    }

    companion object {
        val IDENTITY: ResourceLocation = ResourceLocation(ModInfo.MOD_ID, "machines")
    }

    @SideOnly(Side.CLIENT)
    override fun addSubBlocks(list: NonNullList<ItemStack>, block: BlockTileEntity, item: ItemBlockTileEntity,
                              tab: CreativeTabs) {
        enumValues<TEs>().forEach { if (it.id != -1) list.add(block.getItemStack(it)) }
    }

    // Getters >>
    override fun getExplosionResistance(): Float = _explosionResistance

    override fun getIdentifier(): ResourceLocation = IDENTITY

    override fun getTeClass(): Class<out TileEntityBlock> = _tileEntityClass

    override fun hasActive(): Boolean = _hasActive

    override fun getMaterial(): Material = Material.IRON

    override fun getName(): String = name

    override fun getId(): Int = _itemMeta

    override fun allowWrenchRotating(): Boolean = _allowWrenchRotating

    override fun getPlaceHandler(): TeBlock.ITePlaceHandler? = null

    override fun getRarity(): EnumRarity = _rarity

    override fun getSupportedFacings(): MutableSet<EnumFacing> = _supportedFacings

    override fun getDefaultDrop(): TeBlock.DefaultDrop = _defaultDrop

    override fun isTransparent(): Boolean = true

    override fun setPlaceHandler(placeHandler: TeBlock.ITePlaceHandler?) =
            throw UnsupportedOperationException("This is not currently supported")

    override fun getHarvestTool(): TeBlock.HarvestTool = _harvestTool

    override fun hasItem(): Boolean = _itemMeta != -1

    override fun getHardness(): Float = _hardness

    override fun getDummyTe(): TileEntityBlock? = null
    // << Getters
}