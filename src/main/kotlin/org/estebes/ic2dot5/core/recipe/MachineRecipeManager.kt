package org.estebes.ic2dot5.core.recipe

import com.google.common.base.Equivalence
import org.estebes.ic2dot5.api.recipe.machine.IMachineRecipe
import org.estebes.ic2dot5.api.recipe.machine.IMachineRecipeManager

class MachineRecipeManager : IMachineRecipeManager {
    override fun addRecipe(recipe: IMachineRecipe): Boolean {
        val ret: Boolean = this._rawRecipes.add(recipe)
        recipe.getInputs().forEach {
            this._possibleInputs.add(EquivalenceInstances.customEquivalence.wrap(it))
        }
        return ret
    }

    override fun getRecipes(): MutableCollection<IMachineRecipe> = this._rawRecipes

    override fun acceptsInput(input: Any): Boolean =
            this._possibleInputs.contains(EquivalenceInstances.customEquivalence.wrap(input))

    override fun getOutputFor(inputs: MutableCollection<Any>): MutableMap<Any, Float> {
        val ret = hashMapOf<Any, Float>()
        for (aux in this._rawRecipes) {
            if (aux.getInput().matches(inputs)) {
                ret.putAll(aux.getOutputs())
                break
            }
        }
        return ret
    }

    // Properties >>
    private val _rawRecipes: MutableCollection<IMachineRecipe> = mutableListOf()

    private val _possibleInputs: MutableCollection<Equivalence.Wrapper<Any>> = hashSetOf()
    // << Properties
}