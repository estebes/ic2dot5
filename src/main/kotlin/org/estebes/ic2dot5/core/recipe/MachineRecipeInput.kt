package org.estebes.ic2dot5.core.recipe

import com.google.common.base.Equivalence
import net.minecraft.item.ItemStack
import net.minecraftforge.fluids.FluidStack
import org.estebes.ic2dot5.api.recipe.machine.IMachineRecipeInput

class MachineRecipeInput : IMachineRecipeInput {
    // IMachineRecipeInput >>
    override fun <T : Any> addInput(input: T): Boolean = when (input) {
        is ItemStack -> this._inputs.put(EquivalenceInstances.customEquivalence.wrap(input), input) != null
        is FluidStack -> this._inputs.put(EquivalenceInstances.customEquivalence.wrap(input), input) != null
        else -> throw IllegalArgumentException("Not accepted")
    }

    override fun getInputs(): MutableCollection<Any> = this._inputs.values

    override fun matches(subjects: MutableCollection<*>): Boolean {
        // Eliminate duplicates
        val aux: MutableCollection<Equivalence.Wrapper<*>> = hashSetOf()
        subjects.forEach {
            aux.add(EquivalenceInstances.customEquivalence.wrap(it))
        }

        if (aux.size != this._inputs.size)
            return false

        subjects.forEach {
            if (!_inputs.containsKey(EquivalenceInstances.customEquivalence.wrap(it)))
                return false
        }

        return true
    }

    override fun matchesWithCount(subjects: MutableCollection<*>): Boolean {
        // Eliminate duplicates
        val aux: MutableCollection<Equivalence.Wrapper<*>> = hashSetOf()
        subjects.forEach {
            aux.add(EquivalenceInstances.customEquivalence.wrap(it))
        }

        if (aux.size != this._inputs.size)
            return false

        aux.forEach {
            if (_inputs.containsKey(it)) {
                val entry = it.get()
                when (entry) {
                    is ItemStack -> if (entry.count  < (_inputs[it] as ItemStack).count) return false
                    is FluidStack -> if (entry.amount < (_inputs[it] as FluidStack).amount) return false
                    else -> return false
                }
            } else {
                return false
            }
        }

        return true
    }
    // << IMachineRecipeInput

    // Properties >>
    private val _inputs: MutableMap<Equivalence.Wrapper<*>, Any> = hashMapOf()
    // << Properties
}

/**
 * Helper Builder
 */
fun machineRecipeInput(block: MachineRecipeInput.() -> Unit): MachineRecipeInput {
    val machineRecipeInput = MachineRecipeInput()
    machineRecipeInput.block()
    return machineRecipeInput
}

/*
Input hatch -> iron and copper -> Recipe searches the first available recipeg
 */