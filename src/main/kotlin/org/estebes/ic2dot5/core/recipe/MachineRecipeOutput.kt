package org.estebes.ic2dot5.core.recipe

import com.google.common.base.Equivalence
import org.estebes.ic2dot5.api.recipe.machine.IMachineRecipeOutput

class MachineRecipeOutput : IMachineRecipeOutput {
    // IMachineRecipeOutput >>
    override fun <T : Any> addOutput(output: T): Boolean =
            this.addOutput(output, 1.0F)

    override fun <T : Any> addOutput(output: T, chance: Float): Boolean =
            (this._outputs.put(output, chance) != null) and
                    this._equivalenceOutputs.add(EquivalenceInstances.customEquivalence.wrap(output))

    override fun getOutputs(): MutableMap<Any, Float> = this._outputs

    override fun matches(vararg subjects: Any): Boolean {
        if (subjects.size != this._equivalenceOutputs.size)
            return false

        subjects.forEach {
            if (!_equivalenceOutputs.contains(EquivalenceInstances.customEquivalence.wrap(it)))
                return false
        }

        return true
    }
    // << IMachineRecipeOutput

    // Properties >>
    private val _outputs: MutableMap<Any, Float> = hashMapOf()

    private val _equivalenceOutputs: MutableCollection<Equivalence.Wrapper<Any>> = hashSetOf()
    // << Properties
}