package org.estebes.ic2dot5.core.recipe

import com.google.common.base.Equivalence
import ic2.core.util.StackUtil
import net.minecraft.item.ItemStack
import net.minecraftforge.fluids.FluidStack
import java.util.Objects

object EquivalenceInstances {
    val customEquivalence = CustomEquivalence()
}

class CustomEquivalence : Equivalence<Any>() {
    override fun doHash(subject: Any): Int = when (subject) {
        is ItemStack -> Objects.hash(subject.item.hashCode(),
                subject.itemDamage.hashCode(), subject.tagCompound?.hashCode())
        is FluidStack -> Objects.hash(subject.fluid.hashCode(), subject.tag?.hashCode())
        else -> throw IllegalArgumentException("Not accepted")
    }

    override fun doEquivalent(subjectA: Any, subjectB: Any): Boolean = when {
        (subjectA is ItemStack) and (subjectB is ItemStack) ->
            StackUtil.checkItemEqualityStrict(subjectA as ItemStack, subjectB as ItemStack)
        (subjectA is FluidStack) and (subjectB is FluidStack) ->
            (subjectA as FluidStack).isFluidEqual(subjectB as FluidStack)
        else ->
            false
    }
}