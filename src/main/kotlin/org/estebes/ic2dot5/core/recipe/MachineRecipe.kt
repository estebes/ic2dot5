package org.estebes.ic2dot5.core.recipe

import org.estebes.ic2dot5.api.recipe.machine.IMachineRecipe
import org.estebes.ic2dot5.api.recipe.machine.IMachineRecipeInput
import org.estebes.ic2dot5.api.recipe.machine.IMachineRecipeOutput

class MachineRecipe : IMachineRecipe {
    // IMachineRecipe >>
    override fun <T : Any> addInput(input: T): Boolean = this._input.addInput(input)

    override fun <T : Any> addOutput(output: T): Boolean = this._output.addOutput(output)

    override fun <T : Any> addOutput(output: T, chance: Float): Boolean = this._output.addOutput(output, chance)

    override fun getInputs(): MutableCollection<Any> = this._input.getInputs()

    override fun getOutputs(): MutableMap<Any, Float> = this._output.getOutputs()

    override fun getInput(): IMachineRecipeInput = this._input
    // << IMachineRecipe

    // Properties >>
    private val _input: IMachineRecipeInput = MachineRecipeInput()

    private val _output: IMachineRecipeOutput = MachineRecipeOutput()
    // << Properties
}

/**
 * Builder
 */
fun machineRecipe(block: MachineRecipe.() -> Unit): MachineRecipe {
    val machineRecipe = MachineRecipe()
    machineRecipe.block()
    return machineRecipe
}

inline fun <reified T> IMachineRecipe._getInputsOfType(): Collection<T> = this.getInputs().filter { it is T }.map { it as T }

inline fun <reified T> IMachineRecipe._getOutputsOfType(): Collection<T> = this.getOutputs().filter { it is T }.map { it as T }