package org.estebes.ic2dot5.core.invslot

import ic2.core.block.TileEntityInventory
import ic2.core.block.invslot.InvSlotConsumable
import net.minecraft.item.ItemStack
import org.estebes.ic2dot5.api.recipe.machine.IMachineRecipe
import org.estebes.ic2dot5.api.recipe.machine.IMachineRecipeManager

class SlotMachineInput(
        base: TileEntityInventory,
        name: String,
        count: Int,
        val recipeManager: IMachineRecipeManager
) : InvSlotConsumable(base, name, count) {
    override fun accepts(itemStack: ItemStack): Boolean = recipeManager.acceptsInput(itemStack)

    fun recipeAvailable(): IMachineRecipe? {
        for (recipe in recipeManager.getRecipes()) {
            val ret = this.filterNotNull().toHashSet()
            if (recipe.getInput().matchesWithCount(ret)) {
                return recipe
            }
        }
        return null
    }

    //fun consumeInput():
}