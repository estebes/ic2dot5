package org.estebes.ic2dot5.core

import net.minecraft.util.math.ChunkPos

/**
 * Author: Vitor Andrade
 * Date: 8/10/18
 * Time: 9:33 PM
 */

data class ChunkCoords(
        val dimensionId: Int,
        val pos: ChunkPos
) {
    override fun equals(other: Any?): Boolean {
        val chunkCoords = other as ChunkCoords
        return this.dimensionId == chunkCoords.dimensionId && this.pos.x == chunkCoords.pos.x && this.pos.z == chunkCoords.pos.z
    }

    override fun hashCode(): Int {
        val prime = 31
        var result = 1
        result = prime * result + (this.dimensionId xor this.dimensionId.ushr(32))
        result = prime * result + (this.pos.x xor this.pos.x.ushr(32))
        result = prime * result + (this.pos.z xor this.pos.z.ushr(32))
        return result
    }
}