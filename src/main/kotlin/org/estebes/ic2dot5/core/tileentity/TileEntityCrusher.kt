package org.estebes.ic2dot5.core.tileentity

import ic2.api.recipe.Recipes
import ic2.core.ContainerBase
import ic2.core.IC2
import ic2.core.gui.dynamic.DynamicContainer
import ic2.core.gui.dynamic.DynamicGui
import ic2.core.gui.dynamic.GuiParser
import net.minecraft.client.gui.GuiScreen
import net.minecraft.entity.player.EntityPlayer
import net.minecraft.init.Blocks
import net.minecraft.tileentity.TileEntity
import net.minecraft.util.EnumFacing.EAST
import net.minecraft.util.EnumFacing.NORTH
import net.minecraft.util.EnumFacing.SOUTH
import net.minecraft.util.EnumFacing.WEST
import net.minecraftforge.fml.relauncher.Side
import net.minecraftforge.fml.relauncher.SideOnly
import org.estebes.ic2dot5.api.recipe.machine.MachineRecipeManagers
import org.estebes.ic2dot5.core.recipe.MachineRecipeManager
import org.estebes.ic2dot5.core.recipe.machineRecipe
import org.estebes.ic2dot5.core.tileentity.multiblock.IMultiBlockController
import org.estebes.ic2dot5.core.tileentity.multiblock.IMultiBlockPart

class TileEntityCrusher(
        private val tickRate: Int = 20,
        private var ticker: Int = IC2.random.nextInt(tickRate)
) : TileEntityKineticMachine(8, 2 * 8, MachineRecipeManagers.crusher, 3), IMultiBlockController {
    companion object {
        fun init() {
            MachineRecipeManagers.crusher = MachineRecipeManager()
        }

        fun startRecipes() {
            Recipes.macerator.recipes.forEach {
                machineRecipe {
                    it.input.inputs.forEach {
                        addInput(it)
                    }
                    it.output.forEach {
                        addOutput(it)
                    }
                    MachineRecipeManagers.crusher.addRecipe(this)
                }
            }

        }
    }

    override fun onLoaded() {
        super.onLoaded()

        if (!this.world.isRemote) {
            lookupMB()
        }
    }

    override fun updateEntityServer() {
        super.updateEntityServer()

        if (ticker++ % tickRate == 0) {
            if (this.world.isAreaLoaded(this.pos, 8)) {
                lookupMB()
            }
        }
    }

    private fun lookupMB(): Boolean {
        when (this.facing) {
            NORTH -> {
                for (x in -1..1) {
                    for (y in -1..1) {
                        zLoop@ for (z in 0..2) {
                            val tempPos = this.pos.add(x, y, z)
                            when (tempPos) {
                                this.pos -> continue@zLoop
                                this.pos.offset(this.facing.opposite) -> if (this.world.getBlockState(tempPos).block != Blocks.AIR) { return false }
                                else -> {
                                    val tempTE = this.world.getTileEntity(tempPos)
                                    if (tempTE !is IMultiBlockPart) {
                                        return false
                                    } else {
                                        val tempController = tempTE.getControllerInstance()
                                        if ((tempController != null) and (tempController != this)) {
                                            return false
                                        } else {
                                            tempTE.setControllerInstance(this)
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            SOUTH -> {
                for (x in -1..1) {
                    for (y in -1..1) {
                        zLoop@ for (z in -2..0) {
                            val tempPos = this.pos.add(x, y, z)
                            when (tempPos) {
                                this.pos -> continue@zLoop
                                this.pos.offset(this.facing.opposite) -> if (this.world.getBlockState(tempPos).block != Blocks.AIR) { return false }
                                else -> {
                                    val tempTE = this.world.getTileEntity(tempPos)
                                    if (tempTE !is IMultiBlockPart) {
                                        return false
                                    } else {
                                        val tempController = tempTE.getControllerInstance()
                                        if ((tempController != null) and (tempController != this)) {
                                            return false
                                        } else {
                                            tempTE.setControllerInstance(this)
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            WEST -> {
                for (x in 0..2) {
                    for (y in -1..1) {
                        zLoop@ for (z in -1..1) {
                            val tempPos = this.pos.add(x, y, z)
                            when (tempPos) {
                                this.pos -> continue@zLoop
                                this.pos.offset(this.facing.opposite) -> if (this.world.getBlockState(tempPos).block != Blocks.AIR) { return false }
                                else -> {
                                    val tempTE = this.world.getTileEntity(tempPos)
                                    if (tempTE !is IMultiBlockPart) {
                                        return false
                                    } else {
                                        val tempController = tempTE.getControllerInstance()
                                        if ((tempController != null) and (tempController != this)) {
                                            return false
                                        } else {
                                            tempTE.setControllerInstance(this)
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            EAST -> {
                for (x in -2..0) {
                    for (y in -1..1) {
                        zLoop@ for (z in -1..1) {
                            val tempPos = this.pos.add(x, y, z)
                            when (tempPos) {
                                this.pos -> continue@zLoop
                                this.pos.offset(this.facing.opposite) -> if (this.world.getBlockState(tempPos).block != Blocks.AIR) { return false }
                                else -> {
                                    val tempTE = this.world.getTileEntity(tempPos)
                                    if (tempTE !is IMultiBlockPart) {
                                        return false
                                    } else {
                                        val tempController = tempTE.getControllerInstance()
                                        if ((tempController != null) and (tempController != this)) {
                                            return false
                                        } else {
                                            tempTE.setControllerInstance(this)
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            else -> {
                return false
            }
        }

        println("Multiblock")
        return true
    }

    override fun getCoreTileEntity(): TileEntity = this

    // IHasGui >>
    override fun getGuiContainer(entityPlayer: EntityPlayer): ContainerBase<out TileEntityCrusher> =
            DynamicContainer.create(this, entityPlayer, GuiParser.parse(this.teBlock))

    @SideOnly(Side.CLIENT)
    override fun getGui(entityPlayer: EntityPlayer, isAdmin: Boolean): GuiScreen =
            DynamicGui.create(this, entityPlayer, GuiParser.parse(this.teBlock))
    // << IHasGui

    // Properties >>

    // << Properties
}