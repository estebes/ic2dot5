package org.estebes.ic2dot5.core.tileentity.multiblock

import net.minecraft.tileentity.TileEntity

/**
 * Author: Vitor Andrade
 * Date: 8/11/18
 * Time: 7:06 PM
 */

interface IMultiBlockController {
    fun getCoreTileEntity(): TileEntity
}