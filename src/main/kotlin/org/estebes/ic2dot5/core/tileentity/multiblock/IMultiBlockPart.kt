package org.estebes.ic2dot5.core.tileentity.multiblock

/**
 * Author: Vitor Andrade
 * Date: 8/11/18
 * Time: 7:05 PM
 */

interface IMultiBlockPart {
    fun getControllerInstance(): IMultiBlockController?

    fun setControllerInstance(controller: IMultiBlockController?)

    fun isCasing(): Boolean
}