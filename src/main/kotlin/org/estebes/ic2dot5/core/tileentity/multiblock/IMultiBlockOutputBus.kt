package org.estebes.ic2dot5.core.tileentity.multiblock

/**
 * Author: Vitor Andrade
 * Date: 8/12/18
 * Time: 4:32 PM
 */

interface IMultiBlockOutputBus : IMultiBlockPart {
    fun placeInInventory()
}