package org.estebes.ic2dot5.core.tileentity

import forestry.api.recipes.RecipeManagers
import ic2.api.energy.tile.IKineticSource
import ic2.core.ContainerBase
import ic2.core.IC2
import ic2.core.gui.dynamic.DynamicContainer
import ic2.core.gui.dynamic.DynamicGui
import ic2.core.gui.dynamic.GuiParser
import net.minecraft.client.gui.GuiScreen
import net.minecraft.entity.player.EntityPlayer
import net.minecraft.item.ItemStack
import net.minecraftforge.fml.relauncher.Side
import net.minecraftforge.fml.relauncher.SideOnly
import org.estebes.ic2dot5.api.recipe.machine.MachineRecipeManagers
import org.estebes.ic2dot5.core.ChunkCoords
import org.estebes.ic2dot5.core.IC2dot5
import org.estebes.ic2dot5.core.recipe.MachineRecipeManager
import org.estebes.ic2dot5.core.recipe.machineRecipe

class TileEntityCentrifuge : TileEntityKineticMachine(8, 2 * 8, MachineRecipeManagers.centrifuge, 9) {
    companion object {
        fun init() {
            MachineRecipeManagers.centrifuge = MachineRecipeManager()
        }

        fun startRecipes() {
            RecipeManagers.centrifugeManager.recipes().forEach { centrifugeRecipe ->
                machineRecipe {
                    addInput(centrifugeRecipe.input)
                    centrifugeRecipe.allProducts.forEach {
                        addOutput(it.key, it.value)
                    }
                    MachineRecipeManagers.centrifuge.addRecipe(this)
                }
            }
        }
    }

    override fun updateEntityServer() {
        super.updateEntityServer()

//        val direction1 = this.facing.rotateY()
//        val tileEntity1 = this.world.getTileEntity(this.pos.offset(direction1))
//
//        val direction2 = this.facing.rotateYCCW()
//        val tileEntity2 = this.world.getTileEntity(this.pos.offset(direction2))
//
//        if ((tileEntity1 is IKineticSource) and (tileEntity2 is IKineticSource)) {
//            println("eureka")
//            val oilAmount = IC2dot5.oilHandler.getOilInfo()[ChunkCoords(this.world.provider.dimension, this.world.getChunkFromBlockCoords(this.pos).pos)]
//            println(oilAmount)
//            IC2dot5.oilHandler.setOilInfo(ChunkCoords(this.world.provider.dimension, this.world.getChunkFromBlockCoords(this.pos).pos), 50L, false)
//        }

//        var needsInventoryUpdate = false
//
//        val recipe = this.inputSlot.recipeAvailable()
//
//        val power = when (recipe) {
//            null -> 0
//            else -> rrequestKineticPower(minimumNeeded)
//        }
//
//        if (recipe != null && power >= minimumNeeded) {
//            val outputs = recipe.getOutputs().filterKeys { it is ItemStack }
//            if (outputSlot.canAdd(outputs.keys.filterIsInstance<ItemStack>())) {
//                if (++progress == maxProgress) {
//                    progress = 0
//                    this.inputSlot.consume((recipe.getInputs().first() as ItemStack).count)
//                    outputs.forEach {
//                        if (it.key is ItemStack) {
//                            if (it.value > IC2.random.nextFloat()) {
//                                this.outputSlot.add(it.key as ItemStack)
//                            }
//                        }
//                    }
//                    needsInventoryUpdate = true
//                }
//            } else {
//                progress = 0
//            }
//        } else {
//            progress = 0
//        }
//
//        if (needsInventoryUpdate) {
//            super.markDirty()
//        }
    }

    fun rrequestKineticPower(requested: Int): Int {
        val direction = this.facing
        val tileEntity = this.world.getTileEntity(this.pos.offset(direction))

        if (tileEntity is IKineticSource) {
            return tileEntity.drawKineticEnergy(direction.opposite, requested, false)
        }

        return 0
    }

    // IHasGui >>
    override fun getGuiContainer(entityPlayer: EntityPlayer): ContainerBase<out TileEntityCentrifuge> =
            DynamicContainer.create(this, entityPlayer, GuiParser.parse(this.teBlock))

    @SideOnly(Side.CLIENT)
    override fun getGui(entityPlayer: EntityPlayer, isAdmin: Boolean): GuiScreen =
            DynamicGui.create(this, entityPlayer, GuiParser.parse(this.teBlock))
    // << IHasGui
}