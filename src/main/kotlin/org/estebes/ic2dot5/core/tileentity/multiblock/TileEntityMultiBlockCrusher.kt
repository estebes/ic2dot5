package org.estebes.ic2dot5.core.tileentity.multiblock

import net.minecraft.init.Blocks
import net.minecraft.util.EnumFacing.EAST
import net.minecraft.util.EnumFacing.NORTH
import net.minecraft.util.EnumFacing.SOUTH
import net.minecraft.util.EnumFacing.WEST
import org.estebes.ic2dot5.core.block.machines.io.TileEntityOutputBus

/**
 * Author: Vitor Andrade
 * Date: 8/15/18
 * Time: 3:42 PM
 */

class TileEntityMultiBlockCrusher() : TileEntityMultiBlockBase() {
    override fun lookupMB(): Boolean {
        when (this.facing) {
            NORTH -> {
                for (x in -1..1) {
                    for (y in -1..1) {
                        zLoop@ for (z in 0..2) {
                            val tempPos = this.pos.add(x, y, z)
                            when (tempPos) {
                                this.pos -> continue@zLoop
                                this.pos.offset(this.facing.opposite) -> if (this.world.getBlockState(tempPos).block != Blocks.AIR) { return false }
                                else -> {
                                    val tempTE = this.world.getTileEntity(tempPos)
                                    if (tempTE !is TileEntityOutputBus) {
                                        return false
                                    } else {
//                                        val tempController = tempTE.getControllerInstance()
//                                        if ((tempController != null) and (tempController != this)) {
//                                            return false
//                                        } else {
//                                            tempTE.setControllerInstance(this)
//                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            SOUTH -> {
                for (x in -1..1) {
                    for (y in -1..1) {
                        zLoop@ for (z in -2..0) {
                            val tempPos = this.pos.add(x, y, z)
                            when (tempPos) {
                                this.pos -> continue@zLoop
                                this.pos.offset(this.facing.opposite) -> if (this.world.getBlockState(tempPos).block != Blocks.AIR) { return false }
                                else -> {
                                    val tempTE = this.world.getTileEntity(tempPos)
                                    if (tempTE !is TileEntityOutputBus) {
                                        return false
                                    } else {
//                                        val tempController = tempTE.getControllerInstance()
//                                        if ((tempController != null) and (tempController != this)) {
//                                            return false
//                                        } else {
//                                            tempTE.setControllerInstance(this)
//                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            WEST -> {
                for (x in 0..2) {
                    for (y in -1..1) {
                        zLoop@ for (z in -1..1) {
                            val tempPos = this.pos.add(x, y, z)
                            when (tempPos) {
                                this.pos -> continue@zLoop
                                this.pos.offset(this.facing.opposite) -> if (this.world.getBlockState(tempPos).block != Blocks.AIR) { return false }
                                else -> {
                                    val tempTE = this.world.getTileEntity(tempPos)
                                    if (tempTE !is TileEntityOutputBus) {
                                        return false
                                    } else {
//                                        val tempController = tempTE.getControllerInstance()
//                                        if ((tempController != null) and (tempController != this)) {
//                                            return false
//                                        } else {
//                                            tempTE.setControllerInstance(this)
//                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            EAST -> {
                for (x in -2..0) {
                    for (y in -1..1) {
                        zLoop@ for (z in -1..1) {
                            val tempPos = this.pos.add(x, y, z)
                            when (tempPos) {
                                this.pos -> continue@zLoop
                                this.pos.offset(this.facing.opposite) -> if (this.world.getBlockState(tempPos).block != Blocks.AIR) { return false }
                                else -> {
                                    val tempTE = this.world.getTileEntity(tempPos)
                                    if (tempTE !is TileEntityOutputBus) {
                                        return false
                                    } else {
//                                        val tempController = tempTE.getControllerInstance()
//                                        if ((tempController != null) and (tempController != this)) {
//                                            return false
//                                        } else {
//                                            tempTE.setControllerInstance(this)
//                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            else -> {
                return false
            }
        }

        println("Multiblock")
        return true
    }
    override fun updateEntityServer() {
        super.updateEntityServer()

        if (ticker++ % tickRate == 0) {
            println("looking")
            if (this.world.isAreaLoaded(this.pos, 8)) {
                this.active = lookupMB()
            }
        }
    }

}