package org.estebes.ic2dot5.core.tileentity

import ic2.core.ContainerBase
import ic2.core.IHasGui
import ic2.core.block.TileEntityInventory
import ic2.core.block.invslot.InvSlot
import ic2.core.block.invslot.InvSlot.Access.I
import ic2.core.block.invslot.InvSlot.InvSide.ANY
import ic2.core.gui.dynamic.DynamicContainer
import ic2.core.gui.dynamic.DynamicGui
import ic2.core.gui.dynamic.GuiParser
import net.minecraft.client.gui.GuiScreen
import net.minecraft.entity.player.EntityPlayer
import net.minecraft.util.EnumFacing
import net.minecraft.util.EnumHand
import net.minecraftforge.fml.relauncher.Side
import net.minecraftforge.fml.relauncher.SideOnly
import org.estebes.ic2dot5.core.tileentity.multiblock.IMultiBlockController
import org.estebes.ic2dot5.core.tileentity.multiblock.IMultiBlockPart
import java.util.concurrent.atomic.AtomicReference

/**
 * Author: Vitor Andrade
 * Date: 8/12/18
 * Time: 12:08 PM
 */

class TileEntityMultiBlockItemInputHatch(
        private val controllerReference: AtomicReference<IMultiBlockController> = java.util.concurrent.atomic.AtomicReference(),
        private val tickRate: Int = 20,
        private var ticker: Int = ic2.core.IC2.random.nextInt(tickRate)
) : TileEntityInventory(), IMultiBlockPart, IHasGui {
    override fun updateEntityServer() {
        super.updateEntityServer()

        if (ticker++ % tickRate == 0) {
            if (controllerReference.get() != null) {
                if (this.world.getTileEntity(controllerReference.get().getCoreTileEntity().pos) != controllerReference.get()) {
                    controllerReference.set(null)
                }
            }
        }
    }

    override fun onActivated(player: EntityPlayer, hand: EnumHand, side: EnumFacing, hitX: Float, hitY: Float, hitZ: Float): Boolean {
        val reactor = this.controllerReference.get()
        return if (reactor != null) {
            val world = this.getWorld()
            reactor.getCoreTileEntity().blockType.onBlockActivated(world, reactor.getCoreTileEntity().pos, world.getBlockState(reactor.getCoreTileEntity().pos), player, hand, side, hitX, hitY, hitZ)
        } else {
            false
        }
    }

    override fun getControllerInstance(): IMultiBlockController? = controllerReference.get()

    override fun setControllerInstance(controller: IMultiBlockController?) {
        controllerReference.set(controller)
    }

    override fun isCasing(): Boolean = true

    // IHasGui >>
    override fun getGuiContainer(entityPlayer: EntityPlayer): ContainerBase<out TileEntityCrusher> {
        return DynamicContainer.create(controllerReference.get().getCoreTileEntity() as TileEntityCrusher, entityPlayer, GuiParser.parse(this.teBlock))
    }

    @SideOnly(Side.CLIENT)
    override fun getGui(entityPlayer: EntityPlayer, isAdmin: Boolean): GuiScreen =
            DynamicGui.create(controllerReference.get().getCoreTileEntity() as TileEntityCrusher, entityPlayer, GuiParser.parse(this.teBlock))

    override fun onGuiClosed(entityPlayer: EntityPlayer) {
        // NO-OP
    }
    // << IHasGui

    val inputSlot: InvSlot = InvSlot(this, "inputSlot", I, 1, ANY)
}