package org.estebes.ic2dot5.core.tileentity

import ic2.api.energy.tile.IKineticSource
import ic2.core.IC2
import ic2.core.IHasGui
import ic2.core.block.TileEntityInventory
import ic2.core.block.invslot.InvSlot
import ic2.core.block.invslot.InvSlot.Access.I
import ic2.core.block.invslot.InvSlot.InvSide.ANY
import ic2.core.block.invslot.InvSlotOutput
import ic2.core.gui.dynamic.IGuiValueProvider
import ic2.core.network.GuiSynced
import net.minecraft.entity.player.EntityPlayer
import net.minecraft.item.ItemStack
import net.minecraft.nbt.NBTTagCompound
import net.minecraft.util.EnumFacing
import org.estebes.ic2dot5.api.recipe.machine.IMachineRecipeManager
import org.estebes.ic2dot5.api.recipe.machine.MachineRecipeManagers
import org.estebes.ic2dot5.core.invslot.SlotMachineInput

abstract class TileEntityKineticMachine(
        val minimumNeeded: Int,
        val maximum: Int,
        val recipeManager: IMachineRecipeManager,
        val outputSlots: Int = 1
) : TileEntityInventory(), IHasGui, IGuiValueProvider {
    // NBT >>
    override fun readFromNBT(nbtTagCompound: NBTTagCompound) {
        super.readFromNBT(nbtTagCompound)
        this.progress = nbtTagCompound.getInteger("progress")
    }

    override fun writeToNBT(nbtTagCompound: NBTTagCompound): NBTTagCompound {
        super.writeToNBT(nbtTagCompound)
        nbtTagCompound.setInteger("progress", this.progress)
        return nbtTagCompound
    }
    // << NBT

//    override fun updateEntityServer() {
//        super.updateEntityServer()
//
//        if (this.inputSlot.inputAvailable()) {
//            if (this.inputSlot.recipeAvailable()) {
//
//            }
//        }
//
//        if (++progress == maxProgress) {
//            progress = 0
//            if (!inputSlot.get().isEmpty) {
//                this.recipeManager.getOutputFor(hashSetOf(inputSlot.get())).forEach {
//                    if (it.key is ItemStack) {
//                        if (it.value > IC2.random.nextFloat()) {
//                            this.outputSlot.add(it.key as ItemStack)
//                        }
//                    }
//                }
//                this.inputSlot.get().count -= 1
//            }
//        }
//    }

    fun isKineticPowerAvailable(minimumNeeded: Int): Boolean {
        val direction = this.facing
        val tileEntity = this.world.getTileEntity(this.pos.offset(direction))

        if (tileEntity is IKineticSource) {
            val maxBandwidth = tileEntity.drawKineticEnergy(direction.opposite, this.maximum, true)
            if (maxBandwidth < minimumNeeded) return false
        }

        return true
    }

    fun requestKineticPower(requested: Int): Int {
        val direction = this.facing
        val tileEntity = this.world.getTileEntity(this.pos.offset(direction))

        if (tileEntity is IKineticSource) {
            val maxBandwidth = tileEntity.drawKineticEnergy(direction.opposite, this.maximum, true)
            if (maxBandwidth < minimumNeeded) return 0

            return tileEntity.drawKineticEnergy(direction.opposite, this.maximum, false)
        }

        return 0
    }

    // IHasGui >>
    override fun onGuiClosed(entityPlayer: EntityPlayer) {
        // NO-OP
    }
    // << IHasGui

    // IGuiValueProvider >>
    override fun getGuiValue(name: String): Double = when (name) {
        "progress" -> progress.toDouble() / maxProgress.toDouble()
        else -> throw IllegalArgumentException(this.javaClass.simpleName + " Cannot get gui value for " + name)
    }
    // << IGuiValueProvider

    // Properties >>
    @GuiSynced
    var progress: Int = 0

    val maxProgress: Int = 100

    //val inputSlot: SlotMachineInput = SlotMachineInput(this, "input", 1, MachineRecipeManagers.centrifuge)
    val inputSlot: InvSlot = InvSlot(this, "input", I, 1, ANY)

    val outputSlot: InvSlotOutput = InvSlotOutput(this, "output", outputSlots)
    // << Properties
}