package org.estebes.ic2dot5.core.tileentity.multiblock

import ic2.core.IC2
import ic2.core.block.TileEntityBlock
import net.minecraft.nbt.NBTTagCompound
import net.minecraft.tileentity.TileEntity
import org.estebes.ic2dot5.core.block.machines.io.TileEntityInputBus
import org.estebes.ic2dot5.core.block.machines.io.TileEntityOutputBus

/**
 * Author: Vitor Andrade
 * Date: 8/15/18
 * Time: 10:56 AM
 */

abstract class TileEntityMultiBlockBase(
        // Tick rate >>
        protected val tickRate: Int = 20,
        protected var ticker: Int = IC2.random.nextInt(tickRate),
        // << Tick rate

        // Progress >>
        var progress: Int = 0,
        // << Progress

        // Multi-Block parts >>
        val mbInputBuses: ArrayList<TileEntityInputBus> = arrayListOf(),
        val mbOutputBuses: ArrayList<TileEntityOutputBus> = arrayListOf(),
        val mbInputPorts: ArrayList<Int> = arrayListOf(),
        val mbOutputPorts: ArrayList<Int> = arrayListOf()
        // << Multi-Block parts
) : TileEntityBlock() {
    // NBT >>
    override fun readFromNBT(nbtTagCompound: NBTTagCompound) {
        super.readFromNBT(nbtTagCompound)
        this.progress = nbtTagCompound.getInteger("progress")
    }

    override fun writeToNBT(nbtTagCompound: NBTTagCompound): NBTTagCompound {
        super.writeToNBT(nbtTagCompound)
        nbtTagCompound.setInteger("progress", this.progress)
        return nbtTagCompound
    }
    // << NBT

    override fun onLoaded() {
        super.onLoaded()

        if (!this.world.isRemote) {
            mbInputBuses.clear()
            mbOutputBuses.clear()

        }
    }

    // Multi-Block logic >>
    protected abstract fun lookupMB(): Boolean

    protected fun addMultiBlockParts(tileEntity: TileEntity) {
        when (tileEntity) {
            is TileEntityOutputBus -> mbOutputBuses.add(tileEntity)
            else -> throw IllegalArgumentException("Something went wrong")
        }
    }
    // << Multi-Block logic
}