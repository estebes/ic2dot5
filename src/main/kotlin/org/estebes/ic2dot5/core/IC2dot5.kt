package org.estebes.ic2dot5.core

import ic2.api.event.TeBlockFinalCallEvent
import ic2.core.block.TeBlockRegistry
import net.minecraftforge.common.MinecraftForge
import net.minecraftforge.fml.common.Mod
import net.minecraftforge.fml.common.event.FMLConstructionEvent
import net.minecraftforge.fml.common.event.FMLInitializationEvent
import net.minecraftforge.fml.common.event.FMLPostInitializationEvent
import net.minecraftforge.fml.common.event.FMLPreInitializationEvent
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent
import org.estebes.ic2dot5.core.block.BlockWall2
import org.estebes.ic2dot5.core.block.misc.BlockCasing
import org.estebes.ic2dot5.core.event.OilHandler
import org.estebes.ic2dot5.core.ref.ModInfo
import org.estebes.ic2dot5.core.ref.TEs
import org.estebes.ic2dot5.core.tileentity.TileEntityCentrifuge
import org.estebes.ic2dot5.core.tileentity.TileEntityCrusher

@Mod(modid = ModInfo.MOD_ID, name = ModInfo.MOD_NAME, version = ModInfo.MOD_VERSION,
        dependencies = "required-after:forge@[14.23.0.2491,);required-after:ic2@[2.8.96,);required-after:forestry@[5.7.0.214,);")
class IC2dot5 {
    companion object {
        @Mod.Instance(ModInfo.MOD_ID)
        lateinit var instance: IC2dot5

        lateinit var oilHandler: OilHandler
    }

    @Mod.EventHandler
    fun start(event: FMLConstructionEvent) {
        MinecraftForge.EVENT_BUS.register(this)
    }

    @Mod.EventHandler
    fun preInit(event: FMLPreInitializationEvent) {
        oilHandler = OilHandler()

        TileEntityCrusher.init()
        TileEntityCentrifuge.init()

        MinecraftForge.EVENT_BUS.register(oilHandler)

        BlockCasing.create()
    }

    @Mod.EventHandler
    fun init(event: FMLInitializationEvent) {
        // NO-OP
    }

    @Mod.EventHandler
    fun postInit(event: FMLPostInitializationEvent) {
        TileEntityCrusher.startRecipes()
        TileEntityCentrifuge.startRecipes()
    }

    @SubscribeEvent
    fun register(event: TeBlockFinalCallEvent) {
        TeBlockRegistry.addAll(TEs::class.java, TEs.IDENTITY)
    }
}