package org.estebes.ic2dot5.core.event

import ic2.core.IC2
import ic2.core.util.Log
import ic2.core.util.LogCategory.General
import net.minecraft.nbt.NBTTagCompound
import net.minecraft.world.World
import net.minecraft.world.chunk.IChunkProvider
import net.minecraft.world.gen.IChunkGenerator
import net.minecraftforge.event.world.ChunkDataEvent
import net.minecraftforge.event.world.ChunkEvent
import net.minecraftforge.fml.common.IWorldGenerator
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent
import org.estebes.ic2dot5.core.ChunkCoords
import org.estebes.ic2dot5.core.ref.ModInfo
import java.util.Random
import java.util.concurrent.ConcurrentHashMap

/**
 * Author: Vitor Andrade
 * Date: 8/10/18
 * Time: 7:16 PM
 */

class OilHandler : IWorldGenerator {
    companion object {
        private val oilInfo: ConcurrentHashMap<ChunkCoords, Long> = ConcurrentHashMap()
    }

    override fun generate(random: Random, chunkX: Int, chunkZ: Int, world: World, chunkGenerator: IChunkGenerator, chunkProvider: IChunkProvider) {
        val oilSeed = random.nextLong()

        val chunk = chunkProvider.provideChunk(chunkX, chunkZ)

        val chunkCoord = ChunkCoords(dimensionId = world.provider.dimension, pos = chunk.pos)
        oilInfo[chunkCoord] = 666L
    }

    fun getOilInfo(): ConcurrentHashMap<ChunkCoords, Long> = oilInfo

    fun setOilInfo(coords: ChunkCoords, amount: Long, simulate: Boolean): Long {
        if (oilInfo[coords]!! > 0L) {
            oilInfo[coords] = oilInfo[coords]!! - amount
        }
        return amount
    }

    @SubscribeEvent
    fun onChunkLoad(event: ChunkDataEvent.Load) {
        if (!event.world.isRemote) {
            val chunkCoord = ChunkCoords(dimensionId = event.world.provider.dimension, pos = event.chunk.pos)
            if (event.data.hasKey(ModInfo.MOD_ID)) {
                val nbt = event.data.getCompoundTag(ModInfo.MOD_ID)
                when {
                    nbt.hasKey("ic2dot5_oil_amount") -> oilInfo[chunkCoord] = nbt.getLong("ic2dot5_oil_amount")
                    else -> oilInfo[chunkCoord] = 1000L // TODO - enable retro generation
                }
            } else {
                oilInfo[chunkCoord] = 1000L
            }
        }
    }

    @SubscribeEvent
    fun onChunkSave(event: ChunkDataEvent.Save) {
        if (!event.world.isRemote) {
            val nbt = NBTTagCompound()
            val chunkCoord = ChunkCoords(dimensionId = event.world.provider.dimension, pos = event.chunk.pos)
            val oilAmount = oilInfo[chunkCoord]
            if (oilAmount != null)
                nbt.setLong("ic2dot5_oil_amount", oilAmount)

            event.data.setTag(ModInfo.MOD_ID, nbt)
        }
    }

    @SubscribeEvent
    fun onChunkUnload(event: ChunkEvent.Unload) {
        if (!event.world.isRemote) {
            val chunkCoords = ChunkCoords(dimensionId = event.world.provider.dimension, pos = event.chunk.pos)
            oilInfo.remove(chunkCoords)
            IC2.log.info(General, "${ModInfo.MOD_NAME}: Unloading chunk's ($chunkCoords) oil info")
        }
    }
}