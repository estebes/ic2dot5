package org.estebes.ic2dot5.api.recipe.machine

interface IMachineRecipeOutput {
    fun <T : Any> addOutput(output: T): Boolean

    fun <T : Any> addOutput(output: T, chance: Float): Boolean

    fun getOutputs(): MutableMap<Any, Float>

    fun matches(vararg subjects: Any): Boolean
}