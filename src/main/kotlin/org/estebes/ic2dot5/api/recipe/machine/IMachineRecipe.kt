package org.estebes.ic2dot5.api.recipe.machine

interface IMachineRecipe {
    fun <T : Any> addInput(input: T): Boolean

    fun <T : Any> addOutput(output: T): Boolean

    fun <T : Any> addOutput(output: T, chance: Float): Boolean

    fun getInputs(): MutableCollection<Any>

    fun getOutputs(): MutableMap<Any, Float>

    fun getInput(): IMachineRecipeInput

    //fun performInputOutput(subjects: MutableCollection<*>, simulate: Boolean): MutableMap<Any, Float>
}