package org.estebes.ic2dot5.api.recipe.machine

interface IMachineRecipeInput {
    fun <T : Any> addInput(input: T): Boolean

    fun getInputs(): MutableCollection<Any>

    fun matches(subjects: MutableCollection<*>): Boolean

    fun matchesWithCount(subjects: MutableCollection<*>): Boolean
}