package org.estebes.ic2dot5.api.recipe.machine

interface IMachineRecipeManager {
    fun addRecipe(recipe: IMachineRecipe): Boolean

    fun getRecipes(): MutableCollection<IMachineRecipe>

    fun acceptsInput(input : Any): Boolean

    fun getOutputFor(inputs: MutableCollection<Any>): MutableMap<Any, Float>

    //fun getOutputFor(vararg inputs: IMachineRecipeInput<*>): MutableCollection<IMachineRecipeOutput<*>>
}